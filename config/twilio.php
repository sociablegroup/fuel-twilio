<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 * Twilio Package is for connecting and using Twilio services
 *
 * @package    Twilio
 * @version    1.0
 * @author     Michael Bender
 * @license    MIT License
 * @copyright  2012 Infused Industries, Inc.
 * @link       http://bitbucket.org/sociablegroup/fuel-twilio
 */

/**
 * NOTICE:
 *
 * If you need to make modifications to the default configuration, copy
 * this file to your app/config folder, and make them in there.
 *
 * This will allow you to upgrade fuel without losing your custom config.
 */

return array(

    /**
     * Account SID
     */
    'account_sid' => null,

    /**
     * Auth Token
     */
    'auth_token' => null,

    /**
     * Your Twilio Number
     */
    'number' => null,

    /**
     * API Version
     */
    'version' => '2010-04-01',

    /**
     * API Endpoint
     */
    'api_endpoint' => 'https://api.twilio.com'

);
