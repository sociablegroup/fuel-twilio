<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Twilio
 * @version    1.0
 * @author     Michael Bender
 * @license    MIT License
 */


Autoloader::add_core_namespace('Twilio');

Autoloader::add_classes(array(
    'Twilio\\Services_Twilio'                           => __DIR__.'/classes/twilio.php',
    'Twilio\\Services_TwilioException'                 => __DIR__.'/classes/twilio.php',


    'Twilio\\Services_Twilio_ArrayDataProxy'            => __DIR__.'/classes/twilio/ArrayDataProxy.php',
    'Twilio\\Services_Twilio_AutoPagingIterator'        => __DIR__.'/classes/twilio/AutoPagingIterator.php',
    'Twilio\\Services_Twilio_CachingDataProxy'          => __DIR__.'/classes/twilio/CachingDataProxy.php',
    'Twilio\\Services_Twilio_Capability'                => __DIR__.'/classes/twilio/Capability.php',
    'Twilio\\Services_Twilio_DataProxy'                 => __DIR__.'/classes/twilio/DataProxy.php',
    'Twilio\\Services_Twilio_InstanceResource'          => __DIR__.'/classes/twilio/InstanceResource.php',
    'Twilio\\Services_Twilio_ListResource'              => __DIR__.'/classes/twilio/ListResource.php',
    'Twilio\\Services_Twilio_Page'                      => __DIR__.'/classes/twilio/Page.php',
    'Twilio\\Services_Twilio_PartialApplicationHelper'  => __DIR__.'/classes/twilio/PartialApplicationHelper.php',
    'Twilio\\Services_Twilio_ReqeustValidator'          => __DIR__.'/classes/twilio/RequestValidator.php',
    'Twilio\\Services_Twilio_Resource'                  => __DIR__.'/classes/twilio/Resource.php',
    'Twilio\\Services_Twilio_RestException'             => __DIR__.'/classes/twilio/RestException.php',
    'Twilio\\Services_Twilio_TinyHttp'                  => __DIR__.'/classes/twilio/TinyHttp.php',
    'Twilio\\Services_Twilio_Twiml'                     => __DIR__.'/classes/twilio/Twiml.php',

    'Twilio\\Services_Twilio_Rest_Account'              => __DIR__.'/classes/twilio/rest/Account.php',
    'Twilio\\Services_Twilio_Rest_Accounts'             => __DIR__.'/classes/twilio/rest/Accounts.php',
    'Twilio\\Services_Twilio_Rest_Application'          => __DIR__.'/classes/twilio/rest/Application.php',
    'Twilio\\Services_Twilio_Rest_Applications'          => __DIR__.'/classes/twilio/rest/Applications.php',
    'Twilio\\Services_Twilio_Rest_AvailablePhoneNumber' => __DIR__.'/classes/twilio/rest/AvailablePhoneNumber.php',
    'Twilio\\Services_Twilio_Rest_AvailablePhoneNumbers'=> __DIR__.'/classes/twilio/rest/AvailablePhoneNumbers.php',
    'Twilio\\Services_Twilio_Rest_Call'                 => __DIR__.'/classes/twilio/rest/Call.php',
    'Twilio\\Services_Twilio_Rest_Calls'                => __DIR__.'/classes/twilio/rest/Calls.php',
    'Twilio\\Services_Twilio_Rest_Conference'           => __DIR__.'/classes/twilio/rest/Conference.php',
    'Twilio\\Services_Twilio_Rest_Conferences'          => __DIR__.'/classes/twilio/rest/Conferences.php',
    'Twilio\\Services_Twilio_Rest_IncomingPhoneNumber'  => __DIR__.'/classes/twilio/rest/IncomingPhoneNumber.php',
    'Twilio\\Services_Twilio_Rest_IncomingPhoneNumbers' => __DIR__.'/classes/twilio/rest/IncomingPhoneNumbers.php',
    'Twilio\\Services_Twilio_Rest_Notification'         => __DIR__.'/classes/twilio/rest/Notification.php',
    'Twilio\\Services_Twilio_Rest_Notifications'        => __DIR__.'/classes/twilio/rest/Notifications.php',
    'Twilio\\Services_Twilio_Rest_OutgoingCallerId'     => __DIR__.'/classes/twilio/rest/OutgoingCallerId.php',
    'Twilio\\Services_Twilio_Rest_OutgoingCallerIds'    => __DIR__.'/classes/twilio/rest/OutgoingCallerIds.php',
    'Twilio\\Services_Twilio_Rest_Participant'          => __DIR__.'/classes/twilio/rest/Participant.php',
    'Twilio\\Services_Twilio_Rest_Participants'         => __DIR__.'/classes/twilio/rest/Participants.php',
    'Twilio\\Services_Twilio_Rest_Recording'            => __DIR__.'/classes/twilio/rest/Recording.php',
    'Twilio\\Services_Twilio_Rest_Recordings'           => __DIR__.'/classes/twilio/rest/Recordings.php',
    'Twilio\\Services_Twilio_Rest_Sandbox'              => __DIR__.'/classes/twilio/rest/Sandbox.php',
    'Twilio\\Services_Twilio_Rest_ShortCode'            => __DIR__.'/classes/twilio/rest/ShortCode.php',
    'Twilio\\Services_Twilio_Rest_ShortCodes'           => __DIR__.'/classes/twilio/rest/ShortCodes.php',
    'Twilio\\Services_Twilio_Rest_SmsMessage'           => __DIR__.'/classes/twilio/rest/SmsMessage.php',
    'Twilio\\Services_Twilio_Rest_SmsMessages'          => __DIR__.'/classes/twilio/rest/SmsMessages.php',
    'Twilio\\Services_Twilio_Rest_Transcription'        => __DIR__.'/classes/twilio/rest/Transcription.php',
    'Twilio\\Services_Twilio_Rest_Transcriptions'       => __DIR__.'/classes/twilio/rest/Transcriptions.php',
));


/* End of file bootstrap.php */